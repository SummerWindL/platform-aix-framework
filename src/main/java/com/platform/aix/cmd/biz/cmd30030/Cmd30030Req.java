package com.platform.aix.cmd.biz.cmd30030;

import com.platform.aix.cmd.bean.request.BaseCommonRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * @author: Advance
 * @date: 2018/4/18
 * @description:
 */
@Setter
@Getter
@ToString
public class Cmd30030Req extends BaseCommonRequest {


}
